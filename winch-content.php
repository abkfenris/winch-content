<?php
/*
Plugin Name: Winchendon Website Meta content
Plugin URI:
Description: A widget to add additional content to posts on the Winchendon Website. 
Version:
Author: Alex Kerney
Author URI: http://alexkerney.com
*/

// Custom Taxonomy Code from http://wp.tutsplus.com/tutorials/theme-development/innovative-uses-of-wordpress-post-types-and-taxonomies/
add_action( 'init', 'build_taxonomies', 0 );  
  
function build_taxonomies() {  
	register_taxonomy( 'winch_sports_teams', 'post', array( 'hierarchical' => true, 'label' => 'Winchendon Teams', 'query_var' => true, 'rewrite' => true ) );
	register_taxonomy( 'winch_opposing_teams', 'post', array( 'hierarchical' => true, 'label' => 'Opposing Teams', 'query_var' => true, 'rewrite' => true ) );
}  


// Removing the regular metabox so that a metabox with toggles can be added from http://wp.tutsplus.com/tutorials/creative-coding/how-to-use-radio-buttons-with-taxonomies/
add_action( 'admin_menu', 'winch_remove_meta_box');

function winch_remove_meta_box() {
	remove_meta_box('winch_opposing_teamsdiv', 'post', 'normal');
}
add_action( 'add_meta_boxes', 'winch_add_meta_box');
function winch_add_meta_box() {
	add_meta_box( 'winch_opposing_teams', 'Game Information', 'winch_game_metabox', 'post', 'side', 'core');
}
function winch_game_metabox( $post ) {
	// Set up the taxonomy object and get terms
	$taxonomy = 'winch_opposing_teams';
	$tax = get_taxonomy( $taxonomy ); // This is the taxonomy object
	
	// The name of the form
	$name = 'tax_input[' . $taxonomy . ']';
	
	//Get all the terms for this taxonomy
	$terms = get_terms($taxonomy,array('hide_empty' => 0));
	
	$postterms = get_the_terms( $post->ID,$taxonomy );
	$current = ($postterms ? array_pop($postterms) : false);
	$current = ($current ? $current->term_id : 0);
	
	$popular = get_terms( $taxonomy, array( 'orderby' => 'count', 'order' => 'DESC', 'number' => 10, 'hierarchal' => false ) );
	
	// Include fields for the date from http://www.noeltock.com/web-design/wordpress/custom-post-types-events-pt1/

	global $post;
	$custom = get_post_custom($post->ID);
	$meta_gd = $custom["winch_game_date"][0];
	
	$meta_winch_score = isset( $custom['winch_game_score'] ) ? esc_attr( $custom['winch_game_score'][0] ) : '';
	$meta_opposing_score = isset( $custom['winch_game_opposing_score'] ) ? esc_attr( $custom['winch_game_opposing_score'][0] ) : '';
	$meta_was_game = isset( $custom['winch_was_game'] ) ? esc_attr( $custom['winch_was_game'][0] ) : '';
	$meta_winch_game_location = isset( $custom['winch_game_location'] ) ? esc_attr( $custom['winch_game_location'][0] ) : '';
	 
	// - grab wp time format -
	 
	$date_format = get_option('date_format'); // Not required in my code
	$time_format = get_option('time_format');
	 
	// - populate today if empty, 00:00 for time -
	 
	if ($meta_gd == null) { $meta_gd = time();}
	 
	// - convert to pretty formats -
	 
	$clean_gd = date("D, M d, Y", $meta_gd);
	 
	// - security -
	 
	echo '<input type="hidden" name="winch-game-date-nonce" id="winch-game-date-nonce" value="' .
	wp_create_nonce( 'winch-game-date-nonce' ) . '" />';
	 
	// - output -
	 
	?>
	<div class="winch-game-date-meta">
	<ul>
		<li><input type="checkbox" id="winch_was_game" name="winch_was_game" <?php checked( $meta_was_game, 'on' ); ?> /><label for="winch_was_game">Was it a game?</label></li>
		<li>
			<label for="winch_game_location">Game Location</label>
			<select name="winch_game_location" id="winch_game_location">
				<option value="home" <?php selected( $meta_winch_game_location, 'home' ); ?>>Home</option>
				<option value="away" <?php selected( $meta_winch_game_location, 'away' ); ?>>Away</option>
			</select>
		</li>
		<li>
			<label>Game Results:</label>
			<ul>
				<li><label>Winchendon's Score:</label><input name="winch_game_score" class="game_score" value="<?php echo $meta_winch_score; ?>" /></li>
				<li><label>Opposing teams's Score:</label><input name="winch_game_opposing_score" class="game_score" value="<?php echo $meta_opposing_score; ?>" /></li>
			</ul>
		</li>
	    <li class="game-date"><label>Game date: </label><input name="winch_game_date" class="game_date" value="<?php echo $clean_gd; ?>" /></li>
	</ul>
	</div> 	
    <div id="taxonomy-<?php echo $taxonomy; ?>" class="categorydiv">  
  

  
        <!-- Display taxonomy terms -->  
        <div id="<?php echo $taxonomy; ?>-all" class="tabs-panel">  
            <ul id="<?php echo $taxonomy; ?>checklist" class="list:<?php echo $taxonomy?> categorychecklist form-no-clear">  
                <?php   foreach($terms as $term){  
                    $id = $taxonomy.'-'.$term->term_id;  
                    echo "<li id='$id'><label class='selectit'>";  
                    echo "<input type='radio' id='in-$id' name='{$name}'".checked($current,$term->term_id,false)."value='$term->term_id' /> $term->name<br />";  
                   echo "</label></li>";  
                }?>  
           </ul>  
        </div>  
  
    </div>  
    <?php 

}

add_action('save_post', 'save_winch_game_date');

function save_winch_game_date() {

	global $post;
	
	// Require nonce
	
	if( !wp_verify_nonce( $_POST['winch-game-date-nonce'], 'winch-game-date-nonce' ) ) {
		return $post->ID;
	}
	if( !current_user_can( 'edit_post', $post->ID )) {
		return $post->ID;
	}
	
	// Only save game metadata if it was in fact a game, so that hopefully in the future if we wanted to get results we could query by team taxonomy and then get their and opposing scores!
	
	if( isset( $_POST['winch_was_game'] ) ):
		$chk = ( isset( $_POST['winch_was_game'] ) && $_POST['winch_was_game'] ) ? 'on' : 'off';
		update_post_meta( $post->ID, 'winch_was_game', $chk );
	
	
		if( isset( $_POST['winch_game_score'] ) ) update_post_meta( $post->ID, 'winch_game_score', intval( $_POST['winch_game_score'] ) );
		
		if( isset( $_POST['winch_game_opposing_score'] ) ) update_post_meta( $post->ID, 'winch_game_opposing_score', intval( $_POST['winch_game_opposing_score'] ) );
		
		if( isset( $_POST['winch_game_location'] ) ) update_post_meta( $post->ID, 'winch_game_location', esc_attr( $_POST['winch_game_location'] ) );
		
		// convert bad to unix and update post
		if(!isset($_POST['winch_game_date'] ) ):
			return $post;
		endif;
		$updategamed = strtotime( $_POST["winch_game_date"]);
		update_post_meta($post->ID, "winch_game_date", $updategamed );
	endif;

	
	

}

function winch_game_date_styles() {
	global $post_type;
	if( 'post' != $post_type) {
		return;
	}
	wp_enqueue_style('ui-datepicker', get_bloginfo('wpurl') . '/wp-content/plugins/winch-content/css/jquery-ui-1.8.9.custom.css');
	wp_enqueue_style('game-date-meta', get_bloginfo('wpurl') . '/wp-content/plugins/winch-content/css/game-date-meta.css');
}

function winch_game_date_scripts() {
	global $post_type;
	if( 'post' != $post_type ) {
		return;
	}
	wp_enqueue_script('jquery-ui', get_bloginfo('wpurl') . '/wp-content/plugins/winch-content/js/jquery-ui-1.8.9.custom.min.js', array( 'jquery' ) );
	wp_enqueue_script('ui-datepicker', get_bloginfo('wpurl') . '/wp-content/plugins/winch-content/js/jquery.ui.datepicker.js', array( 'jquery-ui' ) );
	wp_enqueue_script('custom_script', get_bloginfo('wpurl').'/wp-content/plugins/winch-content/js/pubforce-admin.js', array( 'jquery' ) );
	
}

add_action( 'admin_print_styles', 'winch_game_date_styles', 1000);
add_action( 'admin_enqueue_scripts', 'winch_game_date_scripts', 1000 );

/* Adding custom styling to the post/page editor from http://wp.tutsplus.com/tutorials/theme-development/adding-custom-styles-in-wordpress-tinymce-editor/ */

// Apply styles to the visual editor
add_filter( 'mce_css', 'winch_content_editor_style' );
function winch_content_editor_style( $mce_css ) {
	if ( ! empty( $mce_css ) )
		$mce_css .= ',';
	// Retrievs the plugin directory URL, change the path is using different directories
	$mce_css .= get_bloginfo('wpurl') . '/wp-content/plugins/winch-content/css/editor-styles.css';
	return $mce_css;
}

// Add "Styles" drop down
add_filter( 'mce_buttons_2', 'winch_content_editor_buttons' );
function winch_content_editor_buttons( $buttons ) {
	array_unshift( $buttons, 'styleselect' );
	return $buttons;
}

// Add styles/classes to the "Styles" drop down menu
add_filter( 'tiny_mce_before_init', 'winch_content_before_init' );
function winch_content_before_init( $settings ) {
	$style_formats = array(
		array(
			'title' => 'Callout Blue',
			'block' => 'div',
			'classes' => 'callout-blue',
			'wrapper' => true
		),
		array(
			'title' => 'Callout Cyan',
			'block' => 'div',
			'classes' => 'callout-cyan',
			'wrapper' => true
		),
		array(
			'title' => 'Callout Dark Green',
			'block' => 'div',
			'classes' => 'callout-dark-green',
			'wrapper' => true
		),
		array(
			'title' => 'Callout Grass Green',
			'block' => 'div',
			'classes' => 'callout-grass-green',
			'wrapper' => true
		),
		array(
			'title' => 'Callout Viewbook Green',
			'block' => 'div',
			'classes' => 'callout-viewbook-green',
			'wrapper' => true
		),
		array(
			'title' => 'Callout Orange',
			'block' => 'div',
			'classes' => 'callout-orange',
			'wrapper' => true
		),
		array(
			'title' => 'Callout Yellow-Orange',
			'block' => 'div',
			'classes' => 'callout-yellow-orange',
			'wrapper' => true
		),
		array(
			'title' => 'Callout Yellow',
			'block' => 'div',
			'classes' => 'callout-yellow',
			'wrapper' => true
		),
		array(
			'title' => 'Callout Grass-Yellow',
			'block' => 'div',
			'classes' => 'callout-grass-yellow',
			'wrapper' => true
		),
		array(
			'title' => 'Callout Winch Green',
			'block' => 'div',
			'classes' => 'callout-winch-green',
			'wrapper' => true
		),
	);
	$settings['style_formats'] = json_encode( $style_formats );
	return $settings;
}

// Add custom stylesheet to the website front-end
add_action( 'wp_enqueue_scripts', 'winch_content_editor_enqueue' );
// Enqueue the stylesheet if it exists
function winch_content_editor_enqueue() {
	$styleURL = plugin_dir_url(__FILE__).'css/editor-styles.css'; // Customstyle.css is relative to the current file
	wp_enqueue_style( 'myCustomStyles', get_bloginfo('wpurl') . '/wp-content/plugins/winch-content/css/editor-styles.css' );
}




?>